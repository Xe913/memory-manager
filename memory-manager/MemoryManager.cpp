#pragma once
#include <iostream>
#include "MemoryManager.h"
#include "SmallObjectAllocator.h"
#include "FreeListAllocator.h"
#include "GeneralPurposeAllocator.h"

#ifndef CHUNK_SIZE
#define CHUNK_SIZE 4096 // 4KB
#endif

#ifndef FLA_MEMORY
#define FLA_MEMORY 1024ULL * 1024 * 100 // 100MB
#endif

#ifndef SMALL_OBJECT_MAX_SIZE
#define SMALL_OBJECT_MAX_SIZE 20
#endif

SmallObjectAllocator soa(CHUNK_SIZE, SMALL_OBJECT_MAX_SIZE);
FreeListAllocator fla(FLA_MEMORY);
GeneralPurposeAllocator gpa;

typedef std::map<void*, std::size_t, std::less<void*>, Mallocator<std::pair<void*, std::size_t>>> SizeMap;
std::map<void*, std::size_t, std::less<void*>, Mallocator<std::pair<void*, std::size_t>>> _sizeMap;

inline bool IsSmallObject(std::size_t size) { return size <= SMALL_OBJECT_MAX_SIZE; }

void* MemoryManager::Allocate(std::size_t size, std::size_t alignment, bool isArray) {
	void* p;
	if (IsSmallObject(size)) p = soa.Allocate(size);
	else p = fla.Allocate(size, alignment);

	if (isArray) _sizeMap.insert({ p, size });
	return p;
}

void MemoryManager::Deallocate(void* p, std::size_t size, bool isArray) {
	if (isArray) {
		SizeMap::iterator it = _sizeMap.find(p);
		if (it != _sizeMap.end()) size = it->second;
	}
	if (IsSmallObject(size)) soa.Deallocate(p, size);
	else fla.Deallocate(p);
}