#pragma once

#include <cassert>
#include "FreeListAllocator.h"

FreeListAllocator::FreeListAllocator(std::size_t size)
	: _freeBlocks((FreeBlock*)malloc(size))
{
	//void* const temp = malloc(size);
	//_freeBlocks = (FreeBlock*)temp;
	assert(size > sizeof(FreeBlock));

	totalSize = size;
	_freeBlocks->size = size;
	_freeBlocks->next = nullptr;
}

FreeListAllocator::~FreeListAllocator() { }

void* FreeListAllocator::Allocate(std::size_t size, std::size_t alignment)
{
	assert(size != 0 && alignment != 0);

	FreeBlock* freeBlock = _freeBlocks;
	FreeBlock* bestFit = nullptr;

	FreeBlock* prevFreeBlock = nullptr;
	FreeBlock* prevBestFit = nullptr;

	std::size_t bestFitAdjustment = 0;
	std::size_t bestFitSize = 0;

	//find best fit
	while (freeBlock != nullptr)
	{
		//calculate alignment
		//std::size_t padding = sizeof(AllocationHeader) + FwdAdjustment(freeBlock + sizeof(AllocationHeader), alignment);
		//add cast to uintptr_t to avoid potential mismatch
		std::size_t adjustment = sizeof(AllocationHeader) + FwdAdjustment((void*)(reinterpret_cast<std::uintptr_t>(freeBlock) + sizeof(AllocationHeader)), alignment);

		std::size_t totalSize = size + adjustment;

		//exact match takes priority
		if (freeBlock->size == totalSize)
		{
			bestFitAdjustment = adjustment;
			bestFitSize = totalSize;
			bestFit = freeBlock;
			prevBestFit = prevFreeBlock;
			break;
		}

		//if no exact match is found, look for the ideal fit
		//TODO: right now only best fit is used. Add options for worst and first fit
		if (freeBlock->size > totalSize && (bestFit == nullptr || freeBlock->size < bestFit->size))
		{
			bestFitAdjustment = adjustment;
			bestFitSize = totalSize;
			bestFit = freeBlock;
			prevBestFit = prevFreeBlock;
		}
		//go to the next freeBlock
		prevFreeBlock = freeBlock;
		freeBlock = freeBlock->next;
	}

	if (bestFit == nullptr)
		return nullptr;

	//If allocations in the remaining memory will be impossible
	//case 1: not enough memory to allocate data -> increase size
	//case 2: enough memory -> create a FreeBlock
	if (bestFit->size - bestFitSize <= sizeof(AllocationHeader))
	{
		bestFitSize = bestFit->size;

		if (prevBestFit != nullptr)
			prevBestFit->next = bestFit->next;
		else
			_freeBlocks = bestFit->next;
	}
	else
	{
		assert(bestFitSize > sizeof(FreeBlock));
		FreeBlock* newBlock = (FreeBlock*)(reinterpret_cast<std::uintptr_t>(bestFit) + bestFitSize);
		newBlock->size = bestFit->size - bestFitSize;
		newBlock->next = bestFit->next;

		if (prevBestFit != nullptr)
			prevBestFit->next = newBlock;
		else
			_freeBlocks = newBlock;
	}

	//create header based on previously calculated aligment data
	std::uintptr_t alignedAddress = (std::uintptr_t)bestFit + bestFitAdjustment;
	AllocationHeader* header = (AllocationHeader*)(alignedAddress - sizeof(AllocationHeader));
	header->size = bestFitSize;
	header->adjustment = bestFitAdjustment;

	//final check: be sure data is correctly aligned
	assert(FwdAdjustment((void*)alignedAddress, alignment) == 0);

#ifdef PRINT_MEMORY_DATA
	std::cout << "FLA - allocating " << size << " bytes with alignment " << alignment << " - total size " << header->size << " with adj " << header->adjustment << std::endl;
#endif
	return (void*)alignedAddress;
}

void FreeListAllocator::Deallocate(void* p)
{
	assert(p != nullptr);
	//find the header by subtracting its size from the pointer
	AllocationHeader* header = (AllocationHeader*)(reinterpret_cast<std::uintptr_t>(p) - sizeof(AllocationHeader));

	//via header, block allocation data (start, end, size) can be calculated
	std::uintptr_t blockStart = reinterpret_cast<std::uintptr_t>(p) - header->adjustment;
	size_t blockSize = header->size;
	std::uintptr_t blockEnd = blockStart + blockSize;
#ifdef PRINT_MEMORY_DATA
	std::cout << "FLA - deallocating " << blockSize << " bytes" << std::endl;
#endif

	FreeBlock* freeBlock = _freeBlocks;
	FreeBlock* prevFreeBlock = nullptr;

	//iterate free blocks until the correct one is found
	while (freeBlock != nullptr)
	{
		//if freeBlock just became bigger than blockEnd, I'm in the correct block
		if ((std::uintptr_t)freeBlock >= blockEnd)
			break;

		prevFreeBlock = freeBlock;
		freeBlock = freeBlock->next;
	}

	//case 1: I'm freeing the first block
	if (prevFreeBlock == nullptr)
	{
		prevFreeBlock = (FreeBlock*)blockStart;
		prevFreeBlock->size = blockSize;
		prevFreeBlock->next = _freeBlocks;
		_freeBlocks = prevFreeBlock;
	}
	//case 2: the previous block is completely free -> I can just add the current block size to its
	else if ((std::uintptr_t)prevFreeBlock + prevFreeBlock->size == blockStart)
	{
		prevFreeBlock->size += blockSize;
	}
	//case 3: none of the above -> block data is added to the freeBlocks structure
	else
	{
		FreeBlock* temp = (FreeBlock*)blockStart;
		temp->size = blockSize;
		temp->next = prevFreeBlock->next;

		prevFreeBlock->next = temp;

		prevFreeBlock = temp;
	}

	assert(prevFreeBlock != nullptr);

	if ((std::uintptr_t)prevFreeBlock + prevFreeBlock->size == (std::uintptr_t)prevFreeBlock->next)
	{
		//TODO: intellisense says this is a null pointer. Must find out why, and why it works. Could be a vs error
		prevFreeBlock->size += prevFreeBlock->next->size;
		prevFreeBlock->next = prevFreeBlock->next->next;
	}
}