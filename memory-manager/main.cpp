#pragma once
#include <iostream>
#include <vector>
#include <ctime>
#include <chrono>
#include "MemoryManagerOverrides.h"

using std::chrono::duration_cast;
using std::chrono::system_clock;

class Test3Int
{
public:
	int _x, _y, _z;
	Test3Int() : _x(0), _y(0), _z(0) { }
	Test3Int(int x, int y, int z) : _x(x), _y(y), _z(z) { }
};

class Test9Float9Double
{
public:
	float _x1, _y1, _z1, _x2, _y2, _z2, _x3, _y3, _z3;
	double _x4, _y4, _z4, _x5, _y5, _z5, _x6, _y6, _z6;
	Test9Float9Double()
		: _x1(0.f), _y1(0.f), _z1(0.f), _x2(0.f), _y2(0.f), _z2(0.f), _x3(0.f), _y3(0.f), _z3(0.f),
		_x4(0.f), _y4(0.f), _z4(0.f), _x5(0.f), _y5(0.f), _z5(0.f), _x6(0.f), _y6(0.f), _z6(0.f)
	{ }
	Test9Float9Double(float x, float y, float z)
		: _x1(x), _y1(y), _z1(z), _x2(x), _y2(y), _z2(z), _x3(x), _y3(y), _z3(z),
		_x4(x), _y4(y), _z4(z), _x5(x), _y5(y), _z5(z), _x6(x), _y6(y), _z6(z)
	{ }
};

class TestCharIntDouble
{
public:
	char _c1, _c2, _c3;
	int _i;
	double _d;
	TestCharIntDouble() : _c1('0'), _c2('0'), _c3('0'), _i(0), _d(0.f) { }
	TestCharIntDouble(char c, int i, float d) : _c1(c), _c2(c), _c3(c), _i(i), _d(d) { }
};

class TestDoubleIntChar
{
public:
	int _i;
	double _d;
	char _c1, _c2, _c3;
	TestDoubleIntChar() : _c1('0'), _c2('0'), _c3('0'), _i(0), _d(0.f) { }
	TestDoubleIntChar(char c, int i, float d) : _c1(c), _c2(c), _c3(c), _i(i), _d(d) { }
};

double StartTimer() {
	return duration_cast<std::chrono::milliseconds>(system_clock::now().time_since_epoch()).count();
}

double StopTimer(long long start) {
	double end = duration_cast<std::chrono::milliseconds>(system_clock::now().time_since_epoch()).count();
	return (end - start) / 1000.0;
}

void TestMassAllocInt() {
	size_t numberOfObjects = 100000;
	std::cout << "Alloc " << numberOfObjects << " Objects composed of 3 ints" << std::endl;

	std::vector<Test3Int*> ptrs;
	ptrs.reserve(numberOfObjects);

	auto start = StartTimer();

	for (int i = 0; i < numberOfObjects; ++i) {
		void* p = MM_NEW(sizeof(Test3Int), alignof(Test3Int));
		ptrs.push_back(new(p) Test3Int(i, i, i));
	}
	for (int i = 0; i < numberOfObjects; ++i) {
		MM_DELETE(ptrs[i], sizeof(Test3Int), alignof(Test3Int));
		ptrs[i]->~Test3Int();
	}

	auto time = StopTimer(start);

	std::cout << "time " << time << std::endl;
}

void TestMassAllocFloatDouble() {
	size_t numberOfObjects = 100000;
	std::cout << "Alloc " << numberOfObjects << " Objects composed of 9 floats and 9 doubles" << std::endl;

	std::vector<Test9Float9Double*> ptrs;
	ptrs.reserve(numberOfObjects);

	auto start = StartTimer();

	for (int i = 0; i < numberOfObjects; ++i) {
		void* p = MM_NEW(sizeof(Test9Float9Double), alignof(Test9Float9Double));
		ptrs.push_back(new(p) Test9Float9Double(i, i, i));
	}
	for (int i = 0; i < numberOfObjects; ++i) {
		MM_DELETE(ptrs[i], sizeof(Test9Float9Double), alignof(Test9Float9Double));
		ptrs[i]->~Test9Float9Double();
	}

	auto time = StopTimer(start);

	std::cout << "time " << time << std::endl;
}

void TestMassAllocCharIntDouble() {
	size_t numberOfObjects = 100000;
	std::cout << "Alloc " << numberOfObjects << " Objects composed of 3 chars, 1 int and 1 double (in this order)" << std::endl;

	std::vector<TestCharIntDouble*> ptrs;
	ptrs.reserve(numberOfObjects);

	auto start = StartTimer();

	for (int i = 0; i < numberOfObjects; ++i) {
		void* p = MM_NEW(sizeof(TestCharIntDouble), alignof(TestCharIntDouble));
		ptrs.push_back(new(p) TestCharIntDouble(i, i, i));
	}
	for (int i = 0; i < numberOfObjects; ++i) {
		MM_DELETE(ptrs[i], sizeof(TestCharIntDouble), alignof(TestCharIntDouble));
		ptrs[i]->~TestCharIntDouble();
	}

	auto time = StopTimer(start);

	std::cout << "time " << time << std::endl;
}

void TestMassAllocDoubleIntChar() {
	size_t numberOfObjects = 100000;
	std::cout << "Alloc " << numberOfObjects << " Objects composed of 1 double, 1 int, 3 chars (in this order)" << std::endl;

	std::vector<TestDoubleIntChar*> ptrs;
	ptrs.reserve(numberOfObjects);

	auto start = StartTimer();

	for (int i = 0; i < numberOfObjects; ++i) {
		void* p = MM_NEW(sizeof(TestDoubleIntChar), alignof(TestDoubleIntChar));
		ptrs.push_back(new(p) TestDoubleIntChar(i, i, i));
	}
	for (int i = 0; i < numberOfObjects; ++i) {
		MM_DELETE(ptrs[i], sizeof(TestDoubleIntChar), alignof(TestDoubleIntChar));
		ptrs[i]->~TestDoubleIntChar();
	}

	auto time = StopTimer(start);

	std::cout << "time " << time << std::endl;
}

void TestMassSmallSizesAlloc()
{
	size_t numberOfObjects = 100000;
	std::cout << "Alloc " << (numberOfObjects * 2) << " Objects composed of 3 ints or 3 chars, 1 int and 1 double (in this order)" << std::endl;

	std::vector<Test3Int*> ptrs1;
	std::vector<TestCharIntDouble*> ptrs2;
	ptrs1.reserve(numberOfObjects);
	ptrs2.reserve(numberOfObjects);

	auto start = StartTimer();

	for (int i = 0; i < numberOfObjects; ++i) {
		void* p1 = MM_NEW(sizeof(Test3Int), alignof(Test3Int));
		ptrs1.push_back(new(p1) Test3Int(i, i, i));
		void* p2 = MM_NEW(sizeof(TestCharIntDouble), alignof(TestCharIntDouble));
		ptrs2.push_back(new(p2) TestCharIntDouble(i, i, i));
	}
	for (int i = 0; i < numberOfObjects; ++i) {
		MM_DELETE(ptrs1[i], sizeof(Test3Int), alignof(Test3Int));
		ptrs1[i]->~Test3Int();
		MM_DELETE(ptrs2[i], sizeof(TestCharIntDouble), alignof(TestCharIntDouble));
		ptrs2[i]->~TestCharIntDouble();
	}

	auto time = StopTimer(start);

	std::cout << "time " << time << std::endl;
}

void TestMassAllSizesAlloc()
{
	size_t numberOfObjects = 100000;
	std::cout << "Alloc " << (numberOfObjects * 4) << " Objects of four different sizes" << std::endl;

	std::vector<Test3Int*> ptrs1;
	std::vector<Test9Float9Double*> ptrs2;
	std::vector<TestCharIntDouble*> ptrs3;
	std::vector<TestDoubleIntChar*> ptrs4;
	ptrs1.reserve(numberOfObjects);
	ptrs2.reserve(numberOfObjects);
	ptrs3.reserve(numberOfObjects);
	ptrs4.reserve(numberOfObjects);

	auto start = StartTimer();

	for (int i = 0; i < numberOfObjects; ++i) {
		void* p1 = MM_NEW(sizeof(Test3Int), alignof(Test3Int));
		ptrs1.push_back(new(p1) Test3Int(i, i, i));
		void* p2 = MM_NEW(sizeof(Test9Float9Double), alignof(Test9Float9Double));
		ptrs2.push_back(new(p2) Test9Float9Double(i, i, i));
		void* p3 = MM_NEW(sizeof(TestCharIntDouble), alignof(TestCharIntDouble));
		ptrs3.push_back(new(p3) TestCharIntDouble(i, i, i));
		void* p4 = MM_NEW(sizeof(TestDoubleIntChar), alignof(TestDoubleIntChar));
		ptrs4.push_back(new(p4) TestDoubleIntChar(i, i, i));
	}
	for (int i = 0; i < numberOfObjects; ++i) {
		MM_DELETE(ptrs1[i], sizeof(Test3Int), alignof(Test3Int));
		ptrs1[i]->~Test3Int();
		MM_DELETE(ptrs2[i], sizeof(Test9Float9Double), alignof(Test9Float9Double));
		ptrs2[i]->~Test9Float9Double();
		MM_DELETE(ptrs3[i], sizeof(TestCharIntDouble), alignof(TestCharIntDouble));
		ptrs3[i]->~TestCharIntDouble();
		MM_DELETE(ptrs4[i], sizeof(TestDoubleIntChar), alignof(TestDoubleIntChar));
		ptrs4[i]->~TestDoubleIntChar();
	}

	auto time = StopTimer(start);

	std::cout << "time " << time << std::endl;
}

void TestAllocIntArray() {
	size_t numberOfObjects = 1000000;
	std::cout << "Alloc an array of " << numberOfObjects << " Objects composed of 3 ints" << std::endl;
	auto start = StartTimer();
	Test3Int* a = MM_NEW_A(Test3Int, numberOfObjects);
	auto time = StopTimer(start);
	std::cout << "time " << time << std::endl;
}

void TestAllocFloatDoubleArray() {
	size_t numberOfObjects = 1000000;
	std::cout << "Alloc an array of " << numberOfObjects << " Objects composed of 9 floats and 9 doubles" << std::endl;
	auto start = StartTimer();
	Test9Float9Double* a = MM_NEW_A(Test9Float9Double, numberOfObjects);
	auto time = StopTimer(start);
	std::cout << "time " << time << std::endl;
}

void TestAllocCharIntDoubleArray() {
	size_t numberOfObjects = 1000000;
	std::cout << "Alloc an array of " << numberOfObjects << " Objects composed of 3 chars, 1 int and 1 double (in this order)" << std::endl;
	auto start = StartTimer();
	TestCharIntDouble* a = MM_NEW_A(TestCharIntDouble, numberOfObjects);
	auto time = StopTimer(start);
	std::cout << "time " << time << std::endl;
}

void TestAllocDoubleIntCharArray() {
	size_t numberOfObjects = 1000000;
	std::cout << "Alloc an array of " << numberOfObjects << " Objects composed of 1 double, 1 int, 3 chars (in this order)" << std::endl;
	auto start = StartTimer();
	TestDoubleIntChar* a = MM_NEW_A(TestDoubleIntChar, numberOfObjects);
	auto time = StopTimer(start);
	std::cout << "time " << time << std::endl;
}

int main(int argc, char* argv[])
{
	TestMassAllocInt();
	TestMassAllocFloatDouble();
	TestMassAllocCharIntDouble();
	TestMassAllocDoubleIntChar();
	TestMassSmallSizesAlloc();
	TestMassAllSizesAlloc();
	TestAllocIntArray();
	TestAllocFloatDoubleArray();
	TestAllocCharIntDoubleArray();
	TestAllocDoubleIntCharArray();
	return 0;
}