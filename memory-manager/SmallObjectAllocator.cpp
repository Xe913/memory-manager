#pragma once
#include <cassert>
#include "SmallObjectAllocator.h"
#include "FixedAllocator.h"

struct CompareFixedAllocatorSize
	: std::binary_function<const FixedAllocator&, std::size_t, bool>
{
	bool operator()(const FixedAllocator& x, std::size_t numBytes) const
	{
		return x.GetBlockSize() < numBytes;
	}
};

SmallObjectAllocator::SmallObjectAllocator(std::size_t chunkSize, std::size_t maxObjectSize)
	: _lastAlloc(0), _lastDealloc(0), _chunkSize(chunkSize), _maxObjectSize(maxObjectSize) {

}

void* SmallObjectAllocator::Allocate(std::size_t numBytes)
{
#ifdef PRINT_MEMORY_DATA
	std::cout << "SOA - allocating " << numBytes << " bytes" << std::endl;
#endif
	//initial test only. This would loop since new is now overridden
	//if (numBytes > maxObjectSize_) return operator new(numBytes);
	if (_lastAlloc && _lastAlloc->GetBlockSize() == numBytes)
	{
		return _lastAlloc->Allocate();
	}
	//gets the first fixed allocator that manages data big enough to contain what I'm trying to allocate
	Pool::iterator i = std::lower_bound(pool_.begin(), pool_.end(), numBytes, CompareFixedAllocatorSize());
	//case 1: no allocator is big enough
	//case 2: an allocator is found, but its bigger than the data size
	//case 3: the perfect allocator is found -> skip this if
	if (i == pool_.end() || i->GetBlockSize() != numBytes)
	{
		//a new allocator is inserted in the last position checked. If none was found, its the bigger yet
		//and its inserted at the end of the list
		//if one was found, but it was bigger than the data size, the new one is placed right before it
		//in the end the list is still ordered
		i = pool_.insert(i, FixedAllocator(numBytes));
		_lastDealloc = &*pool_.begin();
	}
	//updates the last allocation used and allocates
	_lastAlloc = &*i;
	return _lastAlloc->Allocate();
}

void SmallObjectAllocator::Deallocate(void* p, std::size_t numBytes)
{
	//initial test only. This would loop since delete is now overridden
	//if (numBytes > maxObjectSize_) return operator delete(p);

	if (numBytes > _maxObjectSize) return operator delete(p);

	if (_lastDealloc && _lastDealloc->GetBlockSize() == numBytes)
	{
		_lastDealloc->Deallocate(p);
		return;
	}
	Pool::iterator i = std::lower_bound(pool_.begin(), pool_.end(), numBytes, CompareFixedAllocatorSize());
	assert(i != pool_.end());
	assert(i->GetBlockSize() == numBytes);
	//updates last deallocation and deallocate
	_lastDealloc = &*i;
	_lastDealloc->Deallocate(p);
}

std::size_t SmallObjectAllocator::GetMaxSize()
{
	return _maxObjectSize;
}