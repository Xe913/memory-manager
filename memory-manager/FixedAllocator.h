#pragma once
#include <iostream>
#include <vector>
#include "Chunk.h"
#include "Mallocator.h"

#ifndef DEFAULT_CHUNK_SIZE
#define DEFAULT_CHUNK_SIZE 4096
#endif

class FixedAllocator
{
private:
	std::size_t _blockSize;
	unsigned char _numBlocks;
	typedef std::vector<Chunk, Mallocator<FixedAllocator>> Chunks;
	Chunks _chunks;
	Chunk* _allocChunk;
	Chunk* _deallocChunk;
	Chunk* VicinityFind(void* p) const;
	void DoDeallocate(void* p);
	mutable const FixedAllocator* _prev;
	mutable const FixedAllocator* _next;

public:
	explicit FixedAllocator(std::size_t blockSize = 0, std::size_t chunkSize = DEFAULT_CHUNK_SIZE);
	FixedAllocator(const FixedAllocator&);
	FixedAllocator& operator=(const FixedAllocator&);
	~FixedAllocator();
	void* Allocate();
	void Deallocate(void* p);
	void Swap(FixedAllocator& other);
	std::size_t GetBlockSize() const;
	std::size_t GetNumBlocks() const;
};