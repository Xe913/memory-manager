#pragma once
#include <iostream>

struct Chunk
{
	unsigned char* _data;
	unsigned char _firstAvailBlock, _availableBlocks;

	void Init(std::size_t blockSize, unsigned char blocks);
	void* Allocate(std::size_t blockSize);
	void Deallocate(void* p, std::size_t blockSize);
	void Release();

	bool HasBlock(void* p, std::size_t chunkLenght);
	bool HasAvailable(unsigned char numBlocks);
	bool IsFilled();
};