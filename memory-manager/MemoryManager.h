#pragma once
#include <iostream>
#include <string>
#include <map>

class MemoryManager
{
public:
	void* Allocate(std::size_t size, std::size_t alignment, bool isArray = false);
	void Deallocate(void* p, std::size_t size, bool isArray = false);
};