#define USE_MM_NEW
#define USE_MM_DELETE
#define USE_MM_NEW_A
#define USE_MM_DELETE_A

#pragma once
#include <iostream>
#include <new>
#include <string>
#include "MemoryManager.h"

MemoryManager mm;

#ifdef USE_MM_NEW
#define MM_NEW(SIZE, ALIGN) ::operator new(SIZE, ALIGN, __FILE__, __func__, __LINE__);
void* operator new(std::size_t size) { return mm.Allocate(size, alignof(std::max_align_t)); }
void* operator new(std::size_t size, std::size_t alignment, const char* file, const char* func, const unsigned long line) { return mm.Allocate(size, alignment); }
#endif

#ifdef USE_MM_DELETE
#define MM_DELETE(PTR, SIZE, ALIGN) ::operator delete(PTR, SIZE, ALIGN, __FILE__, __func__, __LINE__);
void operator delete(void* p, std::size_t size) { return mm.Deallocate(p, size); }
void operator delete(void* p, std::size_t size, std::size_t alignment, const char* file, const char* func, const unsigned long line) noexcept { return mm.Deallocate(p, size); }
#endif

#ifdef USE_MM_NEW_A
#define MM_NEW_A(T, LEN) new(alignof(T), __FILE__, __func__, __LINE__) T[LEN];
void* operator new[](std::size_t size) { return mm.Allocate(size, alignof(std::max_align_t), true); }
void* operator new[](std::size_t size, std::size_t alignment, const char* file, const char* func, const unsigned long line) { return mm.Allocate(size, alignment, true); }
#endif

#ifdef USE_MM_DELETE_A
#define MM_DELETE_A(PTR, LEN) _deleteA(PTR, LEN, __FILE__, __func__, __LINE__);

template<typename T>
void _deleteA(T* p, std::size_t len, const char* file, const char* func, const unsigned long line) {
	p->~T();
	::operator delete[](p, len, file, func, line);
}

template<typename void*>
void _deleteA(void* p, std::size_t len, const char* file, const char* func, const unsigned long line) {
	::operator delete[](p, len, file, func, line);
}

void operator delete[](void* p) { mm.Deallocate(p, 0, true); }
void operator delete[](void* p, std::size_t len, const char* file, const char* func, const unsigned long line) noexcept { mm.Deallocate(p, 0, true); }
#endif