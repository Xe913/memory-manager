#pragma once
#include <iostream>

//used for initial tests. Just falls back to malloc/free
class GeneralPurposeAllocator
{
public:
	GeneralPurposeAllocator();
	void* Allocate(std::size_t numBytes);
	void Deallocate(void* p, std::size_t size);

private:
	GeneralPurposeAllocator(const GeneralPurposeAllocator&);
};