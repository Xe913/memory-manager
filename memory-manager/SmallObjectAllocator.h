#pragma once
#include <iostream>
#include <vector>
#include "FixedAllocator.h"
#include "Mallocator.h"

class SmallObjectAllocator
{
public:
	SmallObjectAllocator(std::size_t chunkSize, std::size_t maxObjectSize);
	void* Allocate(std::size_t numBytes);
	void Deallocate(void* p, std::size_t size);
	std::size_t GetMaxSize();

private:
	SmallObjectAllocator(const SmallObjectAllocator&);
	//SmallObjectAllocator& operator=(const SmallObjectAllocator&);

	typedef std::vector<FixedAllocator, Mallocator<FixedAllocator>> Pool;
	Pool pool_;
	FixedAllocator* _lastAlloc;
	FixedAllocator* _lastDealloc;
	std::size_t _chunkSize;
	std::size_t _maxObjectSize;
};