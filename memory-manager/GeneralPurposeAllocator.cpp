#pragma once
#include "GeneralPurposeAllocator.h"

GeneralPurposeAllocator::GeneralPurposeAllocator() { }

void* GeneralPurposeAllocator::Allocate(std::size_t numBytes)
{
	return std::malloc(numBytes);
}

void GeneralPurposeAllocator::Deallocate(void* p, std::size_t numBytes)
{
	return std::free(p);
}