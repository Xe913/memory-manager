#pragma once
#include <iostream>

class FreeListAllocator
{
public:
	FreeListAllocator(std::size_t size);
	~FreeListAllocator();

	void* Allocate(std::size_t size, std::size_t alignment);
	void Deallocate(void* p);

private:
	struct AllocationHeader
	{
		std::size_t size;
		std::size_t adjustment;
	};

	struct FreeBlock
	{
		std::size_t size;
		FreeBlock* next;
	};

	static_assert(sizeof(AllocationHeader) >= sizeof(FreeBlock), "sizeof(AllocationHeader) < sizeof(FreeBlock)");

	FreeListAllocator(const FreeListAllocator&);

	FreeBlock* _freeBlocks;
	std::size_t totalSize;
};

inline std::size_t FwdAdjustment(const void* address, std::size_t alignment)
{
	std::size_t padding = alignment - (reinterpret_cast<std::uintptr_t>(address) & static_cast<std::uintptr_t>(alignment - 1));
	if (padding == alignment)
		return 0;
	return padding;
}