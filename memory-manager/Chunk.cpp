#pragma once
#include <cassert>
#include "Chunk.h"

//Note: we talk about "blocks" with "" because there's no real structure
//that divides the memory in logical sections. This "array" is iterated
//via pointer logic with every index positioned blockSize away from the
//previous one. It's actually a single block of reserved memory.

void Chunk::Init(std::size_t blockSize, unsigned char blocks)
{
#ifdef PRINT_MEMORY_DATA
	std::cout << "    CHUNK - creating chunk with " << blocks << " blocks of size  " << blockSize << std::endl;
#endif
	//initial test only. This would loop since new is now overridden
	//pData_ = new unsigned char[blockSize * blocks];
	_data = static_cast<unsigned char*>(std::malloc(blockSize * blocks));
	_firstAvailBlock = 0;
	_availableBlocks = blocks;
	unsigned char i = 0;
	unsigned char* p = _data;
	//write the "next block index" into every block
	//index * blockSize = "block" location
	for (; i != blocks; p += blockSize)
		*p = ++i;
}

void* Chunk::Allocate(std::size_t blockSize)
{
	if (!_availableBlocks) return 0;
	assert((_firstAvailBlock * blockSize) / blockSize == _firstAvailBlock);
	//since every "block" is initialized with the "next block" location, pResult points
	//to the next free "block". That index is saved in firstAvailableBlock, which will
	//be used to keep track of the "block" where the next object will be allocated, and so on
	unsigned char* pResult = _data + (_firstAvailBlock * blockSize);
	_firstAvailBlock = *pResult;
	--_availableBlocks;
#ifdef PRINT_MEMORY_DATA
		std::cout << "    CHUNK - allocating block of size " << blockSize << " - remaining " << (blocksAvailable_ - 0) << std::endl;
#endif
	return pResult;
}

void Chunk::Deallocate(void* p, std::size_t blockSize)
{
	assert(p >= _data);
	unsigned char* toRelease = static_cast<unsigned char*>(p);
	assert((toRelease - _data) % blockSize == 0);
	*toRelease = _firstAvailBlock;
	_firstAvailBlock = static_cast<unsigned char>((toRelease - _data) / blockSize);
	assert(_firstAvailBlock == (toRelease - _data) / blockSize);
	++_availableBlocks;
}

void Chunk::Release()
{
	free(_data);
}

bool Chunk::HasBlock(void* p, std::size_t chunkLength)
{
	return (p >= _data && p < _data + chunkLength);
}

bool Chunk::HasAvailable(unsigned char numBlocks)
{
	return _availableBlocks <  numBlocks;
}

bool Chunk::IsFilled()
{
	return 0 == _availableBlocks;
}