#pragma once
#include <cassert>
#include "FixedAllocator.h"
#include "Chunk.h"

FixedAllocator::FixedAllocator(std::size_t blockSize, std::size_t chunkSize)
	: _blockSize(blockSize), _allocChunk(0), _deallocChunk(0)
{
	assert(_blockSize > 0);
#ifdef PRINT_MEMORY_DATA
	std::cout << "  FIXED - creating allocator with block size " << blockSize << " and chunk size " << chunkSize << std::endl;
#endif
	_prev = _next = this;
	std::size_t numBlocks = chunkSize / blockSize;
	if (numBlocks > UCHAR_MAX) numBlocks = UCHAR_MAX;
	else if (numBlocks == 0) numBlocks = 8 * blockSize;
	_numBlocks = static_cast<unsigned char>(numBlocks);
	assert(_numBlocks == numBlocks);
}

FixedAllocator::FixedAllocator(const FixedAllocator& other)
	: _blockSize(other._blockSize), _numBlocks(other._numBlocks), _chunks(other._chunks)
{
	_prev = &other;
	_next = other._next;
	other._next->_prev = this;
	other._next = this;

	_allocChunk = other._allocChunk
		? &_chunks.front() + (other._allocChunk - &other._chunks.front())
		: 0;

	_deallocChunk = other._deallocChunk
		? &_chunks.front() + (other._deallocChunk - &other._chunks.front())
		: 0;
}

FixedAllocator::~FixedAllocator()
{
	if (_prev != this)
	{
		_prev->_next = _next;
		_next->_prev = _prev;
		return;
	}
	assert(_prev == _next);
	Chunks::iterator i = _chunks.begin();
	for (; i != _chunks.end(); ++i)
	{
		assert(i->_availableBlocks == _numBlocks);
		i->Release();
	}
}

FixedAllocator& FixedAllocator::operator=(const FixedAllocator& rhs)
{
	FixedAllocator copy(rhs);
	copy.Swap(*this);
	return *this;
}

void* FixedAllocator::Allocate()
{
	//if the chunk is full, or there's no chunk, create a new one
	if (_allocChunk == 0 || _allocChunk->_availableBlocks == 0)
	{
#ifdef PRINT_MEMORY_DATA
		std::cout << "  FIXED - allocator for block size " << blockSize_ << " creating new chunk" << std::endl;
#endif
		Chunks::iterator i = _chunks.begin();
		for (;; ++i)
		{
			if (i == _chunks.end())
			{
				_chunks.reserve(_chunks.size() + 1);
				Chunk newChunk;
				newChunk.Init(_blockSize, _numBlocks);
				_chunks.push_back(newChunk);
				_allocChunk = &_chunks.back();
				_deallocChunk = &_chunks.front();
				break;
			}
			if (i->_availableBlocks > 0)
			{
				_allocChunk = &*i;
				break;
			}
		}
	}
	assert(_allocChunk != 0);
	assert(_allocChunk->_availableBlocks > 0);
#ifdef PRINT_MEMORY_DATA
	std::cout << "  FIXED - allocator for block size " << blockSize_ << " allocating memory" << std::endl;
#endif
	return _allocChunk->Allocate(_blockSize);
}


void FixedAllocator::Deallocate(void* p)
{
	assert(!_chunks.empty());
	assert(&_chunks.front() <= _deallocChunk);
	assert(&_chunks.back() >= _deallocChunk);
	_deallocChunk = VicinityFind(p);
	assert(_deallocChunk);
	DoDeallocate(p);
}

Chunk* FixedAllocator::VicinityFind(void* p) const
{
	assert(!_chunks.empty());
	assert(_deallocChunk);

	const std::size_t chunkLength = _numBlocks * _blockSize;

	//look around the last deallocated chunk
	Chunk* lower = _deallocChunk;
	Chunk* higher = _deallocChunk + 1;
	const Chunk* loBound = &_chunks.front();
	const Chunk* hiBound = &_chunks.back() + 1;

	if (higher == hiBound) higher = 0;

	//NOTE: copypasted code. Since this cycle is bound to find something and return
	//at some point, there's no need to evaluate if the iteration must end or not.
	//Seems kinda risky, but it's faster than the implementation I'd use.
	//TODO: find if there's a way to make the cycle fast enough without sacrificing
	//this much consistency
	for (;;)
	{
		if (lower)
		{
			if (p >= lower->_data && p < lower->_data + chunkLength)
			{
				return lower;
			}
			if (lower == loBound) lower = 0;
			else --lower;
		}

		if (higher)
		{
			if (p >= higher->_data && p < higher->_data + chunkLength)
			{
				return higher;
			}
			if (++higher == hiBound) higher = 0;
		}
	}
	//if I get here for any reason, there was a problem
	assert(false);
	return 0;
}

void FixedAllocator::DoDeallocate(void* p)
{
	assert(_deallocChunk->_data <= p);
	assert(_deallocChunk->_data + _numBlocks * _blockSize > p);
	_deallocChunk->Deallocate(p, _blockSize);
#ifdef PRINT_MEMORY_DATA
	std::cout << "  FIXED - allocator for block size " << blockSize_ << " deallocating memory" << std::endl;
#endif
	if (_deallocChunk->_availableBlocks == _numBlocks)
	{
		Chunk& lastChunk = _chunks.back();
		if (&lastChunk == _deallocChunk)
		{
			if (_chunks.size() > 1 && _deallocChunk[-1]._availableBlocks == _numBlocks)
			{
#ifdef PRINT_MEMORY_DATA
				std::cout << "  FIXED - allocator for block size " << blockSize_ << " releasing chunk" << std::endl;
#endif
				lastChunk.Release();
				_chunks.pop_back();
				_allocChunk = _deallocChunk = &_chunks.front();
			}
			return;
		}
		if (lastChunk._availableBlocks == _numBlocks)
		{
#ifdef PRINT_MEMORY_DATA
			std::cout << "  FIXED - allocator for block size " << blockSize_ << " releasing chunk" << std::endl;
#endif
			lastChunk.Release();
			_chunks.pop_back();
			_allocChunk = _deallocChunk;
		}
		else
		{
			std::swap(*_deallocChunk, lastChunk);
			_allocChunk = &_chunks.back();
		}
	}
}

void FixedAllocator::Swap(FixedAllocator& other)
{
	std::swap(_blockSize, other._blockSize);
	std::swap(_numBlocks, other._numBlocks);
	_chunks.swap(other._chunks);
	std::swap(_allocChunk, other._allocChunk);
	std::swap(_deallocChunk, other._deallocChunk);
}

std::size_t FixedAllocator::GetBlockSize() const
{
	return _blockSize;
}

std::size_t FixedAllocator::GetNumBlocks() const
{
	return _numBlocks;
}